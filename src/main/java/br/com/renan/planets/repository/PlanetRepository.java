package br.com.renan.planets.repository;

import br.com.renan.planets.model.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetRepository extends JpaRepository<Planet, Long> {

    Planet findByNome(String nome);

}
