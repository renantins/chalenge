package br.com.renan.planets.controller;

import br.com.renan.planets.model.Planet;
import br.com.renan.planets.repository.PlanetRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

@RestController
@RequestMapping("/api/planets")
public class PlanetController {

    private PlanetRepository planetRepository;

    public PlanetController(PlanetRepository planetRepository) {
        this.planetRepository = planetRepository;
    }

    @PostMapping
    public ResponseEntity<?> createPlanet(@RequestBody Planet planet) throws URISyntaxException {
        Planet result = planetRepository.save(planet);
        return ResponseEntity.created(new URI("/api/planets/" + result.getId()))
                .body(result);
    }

    @GetMapping
    public ResponseEntity<?> getAllPlanets(@RequestParam(value = "name", required = false) String name) {
        if (name != null) {
            Planet planet = planetRepository.findByNome(name);
            if (planet != null) {
                return ResponseEntity.ok().body(Arrays.asList(planet));
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(planetRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPlanet(@PathVariable Long id) {
        if (planetRepository.findById(id).isPresent()) {
            Planet planet = planetRepository.findById(id).get();
            return ResponseEntity.ok().body(planet);
        }
        return ResponseEntity.notFound().build();
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePlanet(@PathVariable Long id) {
        planetRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
