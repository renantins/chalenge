package br.com.renan.planets.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Planet {
    @Id
    private Long id;
    private String nome;
    private String terreno;
    private String clima;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }
}
