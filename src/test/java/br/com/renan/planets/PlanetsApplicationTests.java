package br.com.renan.planets;

import br.com.renan.planets.controller.PlanetController;
import br.com.renan.planets.model.Planet;
import br.com.renan.planets.repository.PlanetRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class PlanetsApplicationTests {

    @Autowired
    private PlanetRepository planetRepository;
    private MockMvc restPlanetMockMvc;
    private Planet planet;
    private static final Long DEFAULT_ID = 1l;
    private static final String DEFAULT_NOME = "Tatooine";
    private static final String DEFAULT_TERRENO = "desert";
    private static final String DEFAULT_CLIMA = "arid";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlanetController planetController = new PlanetController(planetRepository);
        this.restPlanetMockMvc = MockMvcBuilders.standaloneSetup(planetController).build();
    }

    @BeforeEach
    public void initTest() {
        planet = new Planet();
        planet.setId(DEFAULT_ID);
        planet.setClima(DEFAULT_CLIMA);
        planet.setNome(DEFAULT_NOME);
        planet.setTerreno(DEFAULT_TERRENO);
    }

    @Test
    @Transactional
    public void createPlanet() throws Exception {
        int databaseSize = planetRepository.findAll().size();

        restPlanetMockMvc.perform(post("/api/planets").contentType(MediaType.APPLICATION_JSON).content(this.convertToJson(planet))).andExpect(status().isCreated());

        List<Planet> planetList = planetRepository.findAll();
        assertThat(planetList).hasSize(databaseSize + 1);
        Planet testPlanet = planetList.get(planetList.size() - 1);
        assertThat(testPlanet.getId()).isEqualTo(DEFAULT_ID);
        assertThat(testPlanet.getClima()).isEqualTo(DEFAULT_CLIMA);
        assertThat(testPlanet.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testPlanet.getTerreno()).isEqualTo(DEFAULT_TERRENO);
    }

    @Test
    @Transactional
    public void getAllPlanets() throws Exception {
        planetRepository.saveAndFlush(planet);

        restPlanetMockMvc.perform(get("/api/planets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(planet.getId().intValue())))
                .andExpect(jsonPath("$.[*].clima").value(hasItem(DEFAULT_CLIMA)))
                .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
                .andExpect(jsonPath("$.[*].terreno").value(hasItem(DEFAULT_TERRENO)));
    }

    @Test
    @Transactional
    public void getPlanetById() throws Exception {
        planetRepository.saveAndFlush(planet);

        restPlanetMockMvc.perform(get("/api/planets/{id}",planet.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(planet.getId().longValue()))
                .andExpect(jsonPath("$.clima").value(DEFAULT_CLIMA.toString()))
                .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
                .andExpect(jsonPath("$.terreno").value(DEFAULT_TERRENO.toString()));
    }


    @Test
    @Transactional
    public void getPlanetByNameQuery() throws Exception {
        planetRepository.saveAndFlush(planet);

        restPlanetMockMvc.perform(get("/api/planets/").param("name",planet.getNome()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(planet.getId().intValue()))
                .andExpect(jsonPath("$.[*].clima").value(DEFAULT_CLIMA.toString()))
                .andExpect(jsonPath("$.[*].nome").value(DEFAULT_NOME.toString()))
                .andExpect(jsonPath("$.[*].terreno").value(DEFAULT_TERRENO.toString()));
    }

    @Test
    @Transactional
    public void getPlanetByIdNotFound() throws Exception {
        Long planetId = 3l;
        planetRepository.saveAndFlush(planet);

        restPlanetMockMvc.perform(get("/api/planets/{id}",planetId))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void getPlanetByNameQueryNotFound() throws Exception {
        String planetName = "test";
        planetRepository.saveAndFlush(planet);

        restPlanetMockMvc.perform(get("/api/planets/").param("name",planetName))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deletePlanet() throws Exception {
        planetRepository.saveAndFlush(planet);

        int databaseSize = planetRepository.findAll().size();

        restPlanetMockMvc.perform(delete("/api/planets/{id}",planet.getId()))
                .andExpect(status().isOk());

        List<Planet> planetList = planetRepository.findAll();
        assertThat(planetList).hasSize(databaseSize - 1);
    }

    public String convertToJson(Planet planet) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(planet);
        return json;
    }


}

